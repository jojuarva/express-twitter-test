// Configuracion Base
// =============================================================================

//Se establecen los paquetes que se necesitan
var express    = require('express');    // Se realiza la instancia de express
var app        = express();    // Se engloba la aplicacion express dentro de la variable global
var bodyParser = require('body-parser');    
var Twitter = require('twitter');
var config_account = require('./account_configuration.json');
var fs = require('fs');
var fresh = require('fresh');
var md5 = require('md5');

// Se configura la variable app para que pueda usar bodyParser()
// Esto permite obtener data desde una peticion POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//Bloque de configuracion para peticiones CORS
allowCrossDomain = function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.setHeader('Cache-Control', 'public, max-age=1209600');
res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With, if-none-match');
  if ('OPTIONS' === req.method) {
    res.sendStatus(200);
  } else {
    next();
  }
};

app.use(allowCrossDomain);
//Fin del bloque de configuracion para peticiones CORS

var port = process.env.PORT || config_account.nodejsPort;    // Se establece el puerto

// Configuracion de rutas para el API
// =============================================================================
var router = express.Router();              // Establece la instancia del manejador de rutas de express

//Bloqueo de peticiones a la raiz del servicio 
router.get('/', function(req, res) {
    res.send('404: Page not Found', 404) 
});
router.post('/', function(req, res) {
    res.send('404: Page not Found', 404) 
});

var client = new Twitter({
  consumer_key: config_account.consumer_key,
  consumer_secret: config_account.consumer_secret,
  access_token_key: config_account.access_token_key,
  access_token_secret: config_account.access_token_secret
});
 /*
var client = new Twitter({
  consumer_key: 'GtxVQ9IqeuYu3WNYzF0qi46lq',
  consumer_secret: 'iHIHk299RNg0R9u0ENALelYuFOUCH7efvGymAKX7TNWGId7OTM',
  access_token_key: '166357939-2wcbLKFxDaGxkn0T1P1ciFeZiarMXYE6WukwU02O',
  access_token_secret: 'F96qBNFr5dgXq1RBT2ha9QLH6kUZ8q6ckHpK3ZNmHNKBO'
});
 */

/*Metodo para obtener el ultimo tweet y visualizar una data parcializada*/
router.route('/user_timeline')
    .post(function(req, res) {
		var params = {screen_name: config_account.screen_name,count:1};
		client.get('statuses/user_timeline', params, function(error, tweets, response){			
		  if (!error) {
		    //console.log(tweets);
		    var data=tweets[0];
		    //console.log(data);
			var result;
			if (data.retweeted){
				result={ 
	    				account:{
	    							id: data.user.id_str,						//Id de la cuenta
	    							screen_name:data.user.screen_name,			//Nombre de la cuenta
	    							followers_count:data.user.followers_count	//Cantidad de segudores
	    						},
	    				tweet:{
	    						id:data.id_str,								//Id del tweet
	    						text:data.text,								//Contenido del tweet
	    						favorited:data.retweeted_status.favorited,	//Bandera de favorito
	    						favorite_count:data.retweeted_status.favorite_count,			//Numero de veces marcado como favorito
		    					retweeted:data.retweeted_status.retweeted,						//Bander de retweet
		    					retweet_count:data.retweeted_status.retweet_count				//Numero de veces marcado como retweet
	    				},
	    				rateLimit:{
	    							remaining:Number(response.headers["x-rate-limit-remaining"]) //Numero de peticiones restantes
	    						}
					}
			}else{
					result={ 
		    				account:{
		    							id: data.user.id_str,						//Id de la cuenta
		    							screen_name:data.user.screen_name,			//Nombre de la cuenta
		    							followers_count:data.user.followers_count	//Cantidad de segudores
		    						},
		    				tweet:{
		    						id:data.id_str,								//Id del tweet
		    						text:data.text,								//Contenido del tweet
		    						favorited:data.favorited,	//Bandera de favorito
		    						favorite_count:data.favorite_count,			//Numero de veces marcado como favorito
			    					retweeted:data.retweeted,						//Bander de retweet
			    					retweet_count:data.retweet_count,				//Numero de veces marcado como retweet
			    					created:data.created_at.split(" ")[2] +' '+ data.created_at.split(" ")[1] //Dia y mes de creacion del tweet
		    				},
		    				rateLimit:{
		    							remaining:Number(response.headers["x-rate-limit-remaining"])//Numero de peticiones restantes
		    						}
						}
			}

		    res.send(result);
		  }
		});      	

    })
router.route('/lastedPosts')
    .post(function(req, res) {
		var params = {screen_name: config_account.screen_name,count:3};
		client.get('statuses/user_timeline', params, function(error, tweets, response){			
		  if (!error) {
		    //console.log(tweets);
		    var result=[];
		    var data;
		    tweets.forEach(function(tweet){
		    	data=tweet;
				if (tweet.retweeted){
					result.push({ 
		    				account:{
		    							id: data.user.id_str,						//Id de la cuenta
		    							screen_name:data.user.screen_name,			//Nombre de la cuenta
		    							followers_count:data.user.followers_count	//Cantidad de segudores
		    						},
		    				tweet:{
		    						id:data.id_str,								//Id del tweet
		    						text:data.text,								//Contenido del tweet
		    						favorited:data.retweeted_status.favorited,	//Bandera de favorito
		    						favorite_count:data.retweeted_status.favorite_count,			//Numero de veces marcado como favorito
			    					retweeted:data.retweeted_status.retweeted,						//Bander de retweet
			    					retweet_count:data.retweeted_status.retweet_count,				//Numero de veces marcado como retweet
			    					created:data.created_at.split(" ")[2] +' '+ data.created_at.split(" ")[1] //Dia y mes de creacion del tweet
		    				},
		    				rateLimit:{
		    							remaining:Number(response.headers["x-rate-limit-remaining"]) //Numero de peticiones restantes
		    						}
						})
				}else{
						result.push({ 
			    				account:{
			    							id: data.user.id_str,						//Id de la cuenta
			    							screen_name:data.user.screen_name,			//Nombre de la cuenta
			    							followers_count:data.user.followers_count	//Cantidad de segudores
			    						},
			    				tweet:{
			    						id:data.id_str,								//Id del tweet
			    						text:data.text,								//Contenido del tweet
			    						favorited:data.favorited,	//Bandera de favorito
			    						favorite_count:data.favorite_count,			//Numero de veces marcado como favorito
				    					retweeted:data.retweeted,						//Bander de retweet
				    					retweet_count:data.retweet_count,				//Numero de veces marcado como retweet
				    					created:data.created_at.split(" ")[2] +' '+ data.created_at.split(" ")[1] //Dia y mes de creacion del tweet
			    				},
			    				rateLimit:{
			    							remaining:Number(response.headers["x-rate-limit-remaining"])//Numero de peticiones restantes
			    						}
							})
				}

		    })

		    res.setHeader('ETag', md5(result));
				for(var i = 0;i<result.length;i++){
					result[i].ETag = md5(result);	
				}
				
				if(isFresh(req, res)){
					// client has a fresh copy of resource 
				    	res.statusCode = 304
				    	res.end()
				    	return
				}else{
					res.send(result);
				}
		  }
		});      	

    })    

    function isFresh (req, res) {
	  return fresh(req.headers, {
	    'etag': res.getHeader('ETag'),
	    'last-modified': res.getHeader('Last-Modified')
	  })
	}
router.route('/retweet')
    .post(function(req, res) {

		var params = {id:req.body.twId};

		client.post('statuses/retweet', params, function(error, tweets, response){
		  if (!error) {
		    console.log(tweets);
		    res.send(tweets);
		  }else{
		  	console.log(error);
		  }
		});      	

    })

router.route('/home_timeline')
    .get(function(req, res) {
		var params = {screen_name: config_account.screen_name,count:1};
		client.get('statuses/home_timeline', params, function(error, tweets, response){
		  if (!error) {
		    //console.log(tweets);
		    res.send(tweets);
		  }
		});      	

    })
router.route('/mentions_timeline')
    .get(function(req, res) {
		var params = {screen_name: config_account.screen_name,count:1};
		client.get('statuses/mentions_timeline', params, function(error, tweets, response){
		  if (!error) {
		    //console.log(tweets);
		    res.send(tweets);
		  }
		});      	

    })    
router.route('/retweets_of_me')
    .get(function(req, res) {
		var params = {screen_name: config_account.screen_name,count:1};
		client.get('statuses/retweets_of_me', params, function(error, tweets, response){
		  if (!error) {
		    //console.log(tweets);
		    res.send(tweets);
		  }
		});      	

    }) 
router.route('/followers')
    .get(function(req, res) {
		var params = {screen_name: config_account.screen_name,user_id:166357939};
		client.get('users/show', params, function(error, tweets, response){
		  if (!error) {
		    //console.log(tweets);
		    var result= tweets.followers_count;
		    res.send({response:result});
		  }
		});      	

    })        

router.route('/favorites')
    .get(function(req, res) {
		var params = {screen_name: config_account.screen_name,count:1};
		client.get('favorites/list', params,function(error, tweets, response){
		  if(error) console.log(error);
		  res.send(tweets);  // The favorites. 
		  //console.log(response);//(response);  // Raw response object. 
		});     	

    })
    .post(function(req,res){
		var params = {id:req.body.twId};
		client.post('favorites/create', params,function(error, tweets, response){
		  if(error) console.log(error);
		  var result=response.body.retweeted_status;
		  res.send(result);  // The favorites. 
		  //console.log(response);//(response);  // Raw response object. 
		}); 		
    })  

router.route('/stream')
	//ver mejor la documentacion
    .get(function(req, res) {
		var stream = client.stream('statuses/filter', {track: 'javascript'});
		stream.on('data', function(tweet) {
		  console.log(tweet.text);
		});
		 
		stream.on('error', function(error) {
		  console.log(error);
		});  	

    })       
// Registro de rutas del API
// Todas las rutas tienen como path principal /api
app.use('/api', router);

// Inicio del servidor
// =============================================================================
app.listen(port);
console.log('Servicio de twitter corriendo en el puerto:' + port);